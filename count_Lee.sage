# number of compositions up to M
def compositionupto(w,s,M):
    if w>s*M:
        comp=0
    elif s>w:
        comp=0
    elif s<=w:
        comp=0
        for j in range(0, min(s,floor((w-s)/M))+1):
            comp=comp+(-1)^j*binomial(s,j)*binomial(w-j*M-1,s-1)
    return comp

# number of rep
def rep(n,e,v,p,s):
    c=0
    M=floor(p^s/2)
    for i in range(0,min(floor(v/2),e)+1):
        print i
        c=c+binomial(v-2*i, floor(v/2)-i)/2*binomial(n*M-v,e)
        print c
    return c 
   
# number of vectors of fixed Lee weight and fixed support size  
def countgivensupp(n,s,w,q):
    M=floor(q/2)
    if q.mod(2)==0:
        if n==0:
            c=0
        elif s>w:
            c=0
        elif w>s*M:
            c=0
        elif w==s*M:
            c=binomial(n,s)
        elif w<s*M:
            c=0
            for k in range(0, min(s, 2*w/q)+1):
                c=c+binomial(n-k,s-k)*2^(s-k)*binomial(n,k)*compositionupto(w-k*M,s-k,M-1)
    if q.mod(2)==1:
        if n==0:
            c=0
        elif s>w:
            c=0
        elif w>s*M:
            c=0
        elif w<=s*M:
            c=binomial(n,s)*2^s*compositionupto(w,s,M)
    return c
 
#  number of vectors of fixed Lee weight
def countLee(n,w,q):
    if w==0:
       c=1
    else:
        c=0
        for s in range(0,min(n,w)+1):
            c=c+countgivensupp(n,s,w,q)
    return c  
    
    






