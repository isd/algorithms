############################ ISD algorithms in the Hamming metric ############################ 


### n = code length
### k = code dimension
### t = error correction capacity
### R = rate = k/n
### seclevel = fixed security level


############################  ISD over F_2 in the Hamming metric ############################ 


# cost of doing intermediate sums over F_2
def intermediatesum(n,w):
    l=0
    for i in range(2, w+1):
        l=l+binomial(n,i)
    return l  
    
    
    

# GV bound knowing n and k want d
def GV_wantd(n,k):
    d=0
    s=0
    l=0
    while l<n-k:
        d=d+1
        s=s+binomial(n,d-1)
        l=log(s,2)
    return d+1    


 #  work factor of Prange over F_2 
def workfactor_prange_F2(n,k,t):
    gc=(n-k)^2*(n+1)
    ops= gc
    prob=binomial(n,t)/binomial((n-k), t)
    cost=N(log(prob*gc,2))
    KS=k*(n-k)
    print 'Given n=', n, 'k=', k, 't=',t, 'KS=', KS
    print 'cost = 2^',cost, 'bit ops'
        
      
   
 # work factor of Prange over F_2, fixed rate R, fixed security level, and assuming the GV bound  
def workfactor_prange_F2_fixedsec(n,R,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_wantd(n,k)
        t=floor((d-1)/2)
        gc=(n-k)^2*(n+1)
        prob=binomial(n,t)/binomial((n-k), t)
        cost=N(log(prob*gc,2))
    KS=k*(n-k)
    endcost=copy(cost)
    print 'Given n=', n, 'k=', k, 't=',t, 'KS=', KS
    print 'cost = 2^',endcost, 'bit ops'
        
      
    
# set of Dumer over F2
def set_Dumer_F2(l,n,t):
    L=[]
    c = 0
    for v in range(0, min(t,l)+1):
        if (binomial(l,v) <= binomial(n-l,t-v))&(binomial(l+1,min(v+1,t)) > binomial(n-l-1,t-min(v+1,t))):
            c = c +1
            L.append(v)
            #a=0
            #for w in range(v, min(v+M,t)+1):
            #    if countLee(l+1,w,p^m)> countLee(n-l-1,t-w,p^m):
            #        a=1
            #        break
            #if a==1:
            #    L.append(v)
            #    c1 = c1+1
    return L    
 
# cost of Dumer    
def Dumer_F2(n,k,t):
    c=0
    for l in range(1,n):
        for v in set_Dumer_F2(l,n,t):
            L1=binomial(l,v)
            L2=binomial(n-l,t-v)
            cost=intermediatesum(l,v)+L1*ceil(log(L1,2))+intermediatesum(n-l,t-v)+L2*(n-k)+L2*ceil(log(L2,2))
            c=c+cost
    cost= N(log(c,2))
    KS=k*(n-k)
    print 'Given n=', n, 'k=', k, 't=', t,'KS=', KS
    print 'cost= 2^', cost, 'bit ops'
            


#  work factor of Lee-Brickell over F_2
def workfactor_LeeBrickell_F2(n,k,t):
    mincost=100000000000
    for v in range(0, min(t,k)+1):
        sp=binomial(n,t)/(binomial(k,v)*binomial(n-k,t-v))
        ci=(n-k)^2*(n+1)+binomial(k,v)*min(n-k, 2*(t-v+1))*v
        cost=N(log(sp*ci,2))
        if cost<mincost:
            mincost=cost
            bestv=v
    KS=k*(n-k)
    print 'Given n=', n, 'k=', k, 't=', t, 'v=', bestv, 'KS=', KS
    print 'cost= 2^', mincost, 'bit ops'
     
    
    
    
#  work factor of Lee-Brickell  over F2, fixed rate R, fixed security level, and assuming the GV bound
def workfactor_LeeBrickell_F2_fixedsec(n,R,seclevel):    
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_wantd(n,k)
        t=floor((d-1)/2)
        mincost=100000000000
        for v in range(0, min(t,k)+1):
            sp=binomial(n,t)/(binomial(k,v)*binomial(n-k,t-v))
            ci=(n-k)^2*(n+1)+binomial(k,v)*min(n-k, 2*(t-v+1))*v
            cost=N(log(sp*ci,2))
            if cost<mincost:
                mincost=cost
                bestv=v
        cost=copy(mincost)
    endcost=copy(cost)
    KS=k*(n-k)
    print 'Given n=', n, 'k=', k, 't=', t, 'v=', bestv, 'KS=', KS
    print 'cost= 2^', endcost, 'bit ops'
    
    
      

    

    
      
#   work factor of Stern over F_2   
def workfactor_stern_F2(n,k,t):
    m1 = floor(k/2)
    m2= k-m1
    mincost= 10000000
    bestv=0
    bestl=0
    for l in range(0, n-k+1):
        for v in range(max(0,ceil((-n+k+l+t)/2)), min(m1, floor(t/2))+1): 
            gc=(n-k)^2*(n+1)
            st=l*(intermediatesum(m1,v)+intermediatesum(m2,v)+binomial(m2,v)) 
            nc=((binomial(m1,v)*binomial(m2,v))/(2^l))*min(n-k-l, 2*(t-2*v+1))*2*v
            ops= gc+st+nc
            prob=binomial(n,t)/(binomial(m1,v)*binomial(m2,v)*binomial(n-k-l, t-2*v))
            cost=N(log(prob*ops,2))
            if cost<mincost:
                mincost=cost
                bestv=v
                bestl=l
    cost=mincost
    v=bestv
    l=bestl
    KS=k*(n-k)
    print 'Given n=', n, 'k=', k, 't=',t, 'v= ',v,'l=',l, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
    
    
     
#   work factor of Stern over F_2, fixed rate R, fixed security level,  and assuming the GV bound
def workfactor_stern_F2_fixedsec(n,R,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_wantd(n,k)
        t=floor((d-1)/2)
        m1 = floor(k/2)
        m2= k-m1
        mincost= 10000000
        bestv=0
        bestl=0
        for l in range(0, n-k+1):
            for v in range(max(0,ceil((-n+k+l+t)/2)), min(m1, floor(t/2))+1): 
                gc=(n-k)^2*(n+1)
                st=l*(intermediatesum(m1,v)+intermediatesum(m2,v)+binomial(m2,v))  
                nc=((binomial(m1,v)*binomial(m2,v))/(2^l))*min(n-k-l, 2*(t-2*v+1))*2*v
                ops= gc+st+nc
                prob=binomial(n,t)/(binomial(m1,v)*binomial(m2,v)*binomial((n-k-l), t-2*v))
                cost=N(log(prob*ops,2))
                if cost<mincost:
                    mincost=cost
                    bestv=v
                    bestl=l
        cost=copy(mincost)
    endcost=copy(cost)
    v=bestv
    l=bestl
    KS= k*(n-k)
    print 'Given n= ', n, 'k=', k, 't=',t, 'v=', v, 'l=', l, 'KS=', KS
    print 'cost= 2^',endcost, 'bit ops'
    
        
     


############################  ISD algorithms over F_q in the Hamming metric ############################ 


##  cost of doing intermediate sums over F_q
def intermediatesum_F_q(q,n,t):
    L=0
    for i in range(2,t+1):
        L=L+binomial(n,i)*(q-1)^i
    return L

# size of Hamming sphere
def Hamming_sphere(n,d,q):
    s=0
    for i in range(0,d+1):
        s=s+binomial(n,i)*(q-1)^i
    return s 
  
# GV bound over F_q
def GV_Fq_wantd(q,n,k):
    d=0
    s=Hamming_sphere(n-1,d-2,q)
    while s<q^(n-k):
        d=d+1
        s=Hamming_sphere(n-1,d-2,q)
    return d-1    

#  workfactor of Prange over F_q
def workfactor_prange_Fq(q,n,k,t):
    gc=(n-k)^2*(n+1)*(log(q,2)^2+log(q,2))
    prob=binomial(n,t)/binomial((n-k), t)    
    cost=N(log(prob*gc,2))
    KS=N(k*(n-k)*log(q,2))
    print 'Given n= ', n, 'k=', k, 't=',t, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'     
     
    
# workfactor of Prange over F_q, fixed rate R, fixed security level,  and assuming the GV bound
def workfactor_prange_Fq_fixedsec(q,n,R,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_Fq_wantd(q,n,k)
        t=floor((d-1)/2)
        gc=(n-k)^2*(n+1)*(log(q,2)^2+log(q,2))
        prob=binomial(n,t)/binomial((n-k), t)    
        cost=N(log(prob*gc,2))
    endcost=copy(cost)
    KS=N(k*(n-k)*log(q,2))
    print 'Given n= ', n, 'k=', k, 't=',t, 'KS=', KS
    print 'cost= 2^',endcost, 'bit ops'
    
   
# set of Dumer over Fq
def set_Dumer_Fq(l,n,t,q):
    L=[]
    c = 0
    for v in range(0, min(t,l)+1):
        if (binomial(l,v)*(q-1)^v <= binomial(n-l,t-v)*(q-1)^(t-v))&(binomial(l+1,min(v+1,t))*(q-1)^(min(v+1,t)) > binomial(n-l-1,t-min(v+1,t))*(q-1)^(min(v+1,t))):
            c = c +1
            L.append(v)
    return L    
 
# cost of Dumer    
def Dumer_Fq(n,k,t,q):
    c=0
    for l in range(1,n):
        for v in set_Dumer_Fq(l,n,t,q):
            L1=binomial(l,v)*(q-1)^v
            L2=binomial(n-l,t-v)*(q-1)^(t-v)
            cost=intermediatesum_F_q(q,l,v)*log(q,2)+L1*ceil(log(L1,2))+intermediatesum_F_q(q,n-l,t-v)*log(q,2)+L2*(n-k)*log(q,2)+L2*ceil(log(L2,2))
            c=c+cost
    cost= N(log(c,2))
    KS=N(k*(n-k)*log(q,2))
    print 'Given n=', n, 'k=', k, 't=', t,'KS=', KS, 'q=',q
    print 'cost= 2^', cost, 'bit ops'
            

    
    
# workfactor of Lee-Brickell over F_q    
def workfactor_LeeBrickell_Fq(q,n,k,t):
    mincost=100000000000
    for v in range(0, min(t,k)+1):
        sp=binomial(n,t)/(binomial(k,v)*binomial(n-k,t-v))
        gc=(n-k)^2*(n+1)*(log(q,2)^2+log(q,2))
        ops=binomial(k,v)*(q-1)^v*min(n-k,q/(q-1)*(t-v+1))*v*(log(q,2)+log(q,2)^2)
        cost=N(log(sp*(gc+ops),2))
        if cost<mincost:
            mincost=copy(cost)
            bestv=v    
    KS=N(k*(n-k)*log(q,2))    
    print 'Given n=', n, 'k=', k, 't=', t, 'v=', bestv, 'KS=', KS
    print 'cost= 2^', mincost, 'bit ops'  
    
    
# workfactor of Lee-Brickell over F_q,   fixed rate R, fixed security level,  and assuming the GV bound 
def workfactor_LeeBrickell_Fq_fixedsec(q,n,R,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_Fq_wantd(q,n,k)
        t=floor((d-1)/2)
        mincost=100000000000
        for v in range(0, min(t,k)+1):
            sp=binomial(n,t)/(binomial(k,v)*binomial(n-k,t-v))
            gc=(n-k)^2*(n+1)*(log(q,2)^2+log(q,2))
            ops=binomial(k,v)*(q-1)^v*min(n-k,q/(q-1)*(t-v+1))*v*(log(q,2)+log(q,2)^2)
            cost=N(log(sp*(gc+ops),2))
            if cost<mincost:
                mincost=copy(cost)
                bestv=v        
        cost=copy(mincost)
    endcost=copy(cost)    
    KS=N(k*(n-k)*log(q,2))
    print 'Given n=', n, 'k=', k, 't=', t, 'v=', bestv, 'KS=',KS
    print 'cost= 2^', endcost, 'bit ops'
    
    
    

  


#  workfactor of Stern over F_q
def workfactor_stern_Fq(q,n,k,t):
    m1 = floor(k/2)
    m2= k-m1
    mincost= 10000000
    bestv=0
    bestl=0
    for l in range(0, n-k+1):
        for v in range(max(0,ceil((-n+k+l+t)/2)), min(m1, floor(t/2))+1): 
            gc=(n-k)^2*(n+1)*(log(q,2)^2+log(q,2))
            cs= (m1+m2)*l*log(q,2)
            ct=l*(intermediatesum_F_q(q,m1,v)+intermediatesum_F_q(q,m2,v)+binomial(m2,v)*(q-1)^v)*log(q,2) 
            nc=((binomial(m1,v)*(q-1)^(2*v)*binomial(m2,v))/(q^l))*min(n-k-l,q/(q-1)*(t-2*v+1))*2*v*(log(q,2)^2+log(q,2))
            ops= gc+cs+ct+nc
            prob=binomial(n,t)/(binomial(m1,v)*binomial(m2,v)*binomial(n-k-l, t-2*v))
            accost=N(log(prob*ops,2))
            if accost<mincost:
                mincost=copy(accost)
                bestv=v
                bestl=l
    cost=mincost
    v=bestv
    l=bestl
    KS=N(k*(n-k)*log(q,2))
    print 'Given n=', n, 'k=', k, 't=',t, 'v= ',v,'l=',l,  'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
    
    



# workfactor of Stern over F_q,  fixed rate R, fixed security level,  and assuming the GV bound 
def workfactor_stern_Fq_fixedsec(q,n,R,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_Fq_wantd(q,n,k)
        t=floor((d-1)/2)
        m1 = floor(k/2)
        m2= k-m1
        mincost= 10000000
        bestv=0
        bestl=0
        for l in range(0, n-k+1):
            for v in range(max(0,ceil((-n+k+l+t)/2)), min(m1, floor(t/2))+1): 
                gc=(n-k)^2*(n+1)*(log(q,2)^2+log(q,2))
                cs= (m1+m2)*l*log(q,2)
                ct=l*(intermediatesum_F_q(q,m1,v)+intermediatesum_F_q(q,m2,v)+binomial(m2,v)*(q-1)^v)*log(q,2) 
                nc=((binomial(m1,v)*(q-1)^(2*v)*binomial(m2,v))/(q^l))*min(n-k-l,q/(q-1)*(t-2*v+1))*2*v*(log(q,2)^2+log(q,2))
                ops= gc+cs+ct+nc
                prob=binomial(n,t)/(binomial(m1,v)*binomial(m2,v)*binomial(n-k-l, t-2*v))
                accost=N(log(prob*ops,2))
                if accost<mincost:
                    mincost=copy(accost)
                    bestv=v
                    bestl=l
        cost=copy(mincost)
        v=bestv
        l=bestl
    endcost=copy(cost)
    KS=N(k*(n-k)*log(q,2))
    print 'Given n= ', n, 'k=', k, 't=',t, ' v= ',v,'l=',l, 'KS=', KS
    print  'cost= 2^',endcost, 'bit ops'
    
  




############################ ISD algorithms in the Lee metric ############################ 


### n = code length
### k = log_q( | C |) 
### t = error correction capacity
### R = rate = k/n
### seclevel = fixed security level


############################  ISD over Z_4 in the Lee metric ############################ 


### k= k1 +k2/2
    


#  cost of doing intermediate sums over Z_4
def intermediatelee_Z4(n,w):
    l=0
    for i in range(2,w+1):
        l=l+binomial(2*n,i)
    return l
    
# amount of vectors in Z_4^n having Lee weight w    
def sumlee_Z4(n,w):
    l=0
    for i in range(0,w+1):
        l=l+binomial(2*n,i)
    return l
    
# GV over Z_4   in Lee metric  
def GV_Z4_wantd(n,k):
    d=1
    c=sumlee_Z4(n,d-1)
    while 4^k< 4^n/((c-1)*3+1):
        d=d+1
        c=sumlee_Z4(n,d-1)
    return d         
     
# work factor of Prange over Z_4
def workfactor_prange_Z4(n,k1,k2,t):
    gc=2*(n-k1)^2*(n+1)
    prob=binomial(2*n,t)/binomial(2*(n-k1-k2), t)
    cost=N(log(prob*gc,2))
    KS=k1*k2+(2*k1+k2)*(n-k1-k2)
    print 'Given n= ', n, 'k1=', k1,'k2=', k2, 't=',t, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
      
    
# work factor of Prange over Z_4, fixed rate R, fixed security level,and assuming the GV bound, here we choose k1 small
def workfactor_prange_Z4_fixedsec_small(n,R,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_Z4_wantd(n,k)
        t=floor((d-1)/2)
        k1=max(1, ceil(2*k-n+t/2))
        k2=(k-k1)*2
        gc=2*(n-k1)^2*(n+1)
        prob=binomial(2*n,t)/binomial(2*(n-k1-k2), t)
        cost=N(log(prob*gc,2))
    endcost=copy(cost)
    KS=k1*k2+(2*k1+k2)*(n-k1-k2)
    print 'Given n= ', n, 'k=', k,  'k1=', k1,'k2=', k2,  't=',t, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
    
    

    
#  work factor of Prange over Z_4, fixed rate R, fixed security level, and assuming the GV bound, here we choose k1  big
def workfactor_prange_Z4_fixedsec_big(n,R,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_Z4_wantd(n,k)
        t=floor((d-1)/2)
        k2=2
        k1=k-1
        gc=2*(n-k1)^2*(n+1)
        prob=binomial(2*n,t)/binomial(2*(n-k1-k2), t)
        cost=N(log(prob*gc,2))
    endcost=copy(cost)
    KS=k1*k2+(2*k1+k2)*(n-k1-k2)
    print 'Given n= ', n, 'k=', k,  'k1=', k1,'k2=', k2, 't=',t, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
  
    
#  work factor of Lee-Brickell over Z_4       
def workfactor_LeeBrickell_Z4(n,k1,k2,t):
    mincost=100000000
    for v in range(max(0, t-2*(n-k1-k2)), min(t+1,2*(k1+k2)+1)):
        sp=binomial(2*n,t)/(binomial(2*(k1+k2),v)*binomial(2*(n-k1-k2),t-v))
        ci=2*(n-k1)^2*(n+1)+intermediatelee_Z4(k1+k2,v)*2*k2  +binomial(2*(k1+k2),v)*min(t-v+1,n-k1-k2)*2*min(v,k1+k2)
        cost=N(log(sp*ci,2))
        if cost<mincost:
            mincost=copy(cost)
            bestv=v
    KS=k1*k2+(2*k1+k2)*(n-k1-k2)
    print 'Given n=', n, 'k1=', k1, 'k2=',k2, 't=', t, 'v=', bestv, 'KS=', KS
    print 'cost= 2^', mincost, 'bit ops'
    
    
   
    
    
#  work factor of Lee-Brickell over Z_4, fixed rate R, fixed security level, seclevel, and assuming the GV bound, here we fix a small k1   
def workfactor_LeeBrickell_Z4_fixedsec_small(n,R,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_Z4_wantd(n,k)
        t=floor((d-1)/2)
        k1=1
        k2=(k-1)*2
        mincost=100000000
        for v in range(max(0, t-2*(n-k1-k2)), min(t+1,2*(k1+k2)+1)):
            sp=binomial(2*n,t)/(binomial(2*(k1+k2),v)*binomial(2*(n-k1-k2),t-v))
            ci=2*(n-k1)^2*(n+1)+intermediatelee_Z4(k1+k2,v)*2*k2  +binomial(2*(k1+k2),v)*min(t-v+1,n-k1-k2)*2*min(v,k1+k2)
            cost=N(log(sp*ci,2))
            if cost<mincost:
                mincost=copy(cost)
                bestv=v
        cost=copy(mincost)
    endcost=copy(cost)
    KS=k1*k2+(2*k1+k2)*(n-k1-k2)
    print 'Given n=', n, 'k=', k, 'k1=', k1, 'k2=',k2, 't=', t, 'v=', bestv, 'KS=', KS
    print 'cost= 2^', endcost, 'bit ops'
 
 
     

    
    
    
#  work factor of Lee-Brickell over Z_4, fixed rate R, fixed security level,  and assuming the GV bound, here we fix a big k1   
def workfactor_LeeBrickell_Z4_fixedsec_big(n,R,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_Z4_wantd(n,k)
        t=floor((d-1)/2)
        k2=2
        k1=k-1
        mincost=100000000
        for v in range(max(0, t-2*(n-k1-k2)), min(t+1,2*(k1+k2)+1)):
            sp=binomial(2*n,t)/(binomial(2*(k1+k2),v)*binomial(2*(n-k1-k2),t-v))
            ci=2*(n-k1)^2*(n+1)+intermediatelee_Z4(k1+k2,v)*2*k2  +binomial(2*(k1+k2),v)*min(t-v+1,n-k1-k2)*2*min(v,k1+k2)
            cost=N(log(sp*ci,2))
            if cost<mincost:
                mincost=copy(cost)
                bestv=v
        cost=copy(mincost)
    endcost=copy(cost)
    KS=k1*k2+(2*k1+k2)*(n-k1-k2)
    print 'Given n=', n, 'k=', k, 'k1=', k1, 'k2=',k2, 't=', t, 'v=', bestv, 'KS=', KS
    print 'cost= 2^', endcost, 'bit ops'
    
    
   
   
 #  work factor of Stern over Z_4
def workfactor_stern_Z4(n,k1,k2,t):
    m1 = floor((k1+k2)/2)
    m2= k1+k2-m1
    mincost= 10000000
    bestv=0
    bestl=0
    for l in range(0, n-k1-k2+1):
        for v in range(max(0,-n+k1+k2+l+ ceil(t/2)), min(2*m1, floor(t/2))+1):   
            gc=2*(n-k1)^2*(n+1)
            st=(2*l+2*k2)*(intermediatelee_Z4(m1,v)+intermediatelee_Z4(m2,v)+binomial(2*m2,v))
            nc=((binomial(2*m1,v)*binomial(2*m2,v))/(4^(k2+l)))*min(t-2*v+1, n-k1-k2-l)*2*min(2*v,k1+k2)
            ops= gc+st+nc
            prob=binomial(2*n,t)/(binomial(2*m1,v)*binomial(2*m2,v)*binomial(2*(n-k1-k2-l), t-2*v))
            cost=N(log(prob*ops,2))
            if cost<mincost:
                mincost=copy(cost)
                bestv=v
                bestl=l
    cost=copy(mincost)
    v=bestv
    l=bestl
    KS=k1*k2+(2*k1+k2)*(n-k1-k2)
    print 'Given n= ', n, 'k1=', k1,'k2=', k2, 't=',t, 'v= ',v,'l=',l, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
    
    
    
 



 #  work factor of Stern over Z_4, fixed rate R, fixed security level,  and assuming the GV bound, here we choose k1   small
def workfactor_stern_Z4_fixedsec_small(n,R,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_Z4_wantd(n,k)
        t=floor((d-1)/2)
        k1=1
        k2=(k-1)*2
        m1 = floor((k1+k2)/2)
        m2= k1+k2-m1
        mincost= 10000000
        bestv=0
        bestl=0
        for l in range(0, n-k1-k2+1):
            for v in range(max(0,-n+k1+k2+l+ ceil(t/2)), min(2*m1, floor(t/2))+1):   
                gc=2*(n-k1)^2*(n+1)
                st=(2*l+2*k2)*(intermediatelee_Z4(m1,v)+intermediatelee_Z4(m2,v)+binomial(2*m2,v))
                nc=((binomial(2*m1,v)*binomial(2*m2,v))/(4^(k2+l)))*min(t-2*v+1, n-k1-k2-l)*2*min(2*v,k1+k2)
                ops= gc+st+nc
                prob=binomial(2*n,t)/(binomial(2*m1,v)*binomial(2*m2,v)*binomial(2*(n-k1-k2-l), t-2*v))
                cost=N(log(prob*ops,2))
                if cost<mincost:
                    mincost=copy(cost)
                    bestv=v
                    bestl=l
        cost=copy(mincost)
        v=bestv
        l=bestl
    endcost=copy(cost)
    KS=k1*k2+(2*k1+k2)*(n-k1-k2)
    print 'Given n= ', n, 'k1=', k1,'k2=', k2, 't=',t, 'v= ',v,'l=',l, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops' 
    
    
    


 #  work factor of Stern over Z_4, fixed rate R, fixed security level, and assuming the GV bound, here we fix a big choice of k1   
def workfactor_stern_Z4_fixedsec_big(n,R,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_Z4_wantd(n,k)
        t=floor((d-1)/2)
        k2=2
        k1=k-1
        m1 = floor((k1+k2)/2)
        m2= k1+k2-m1
        mincost= 10000000
        bestv=0
        bestl=0
        for l in range(0, n-k1-k2+1):
            for v in range(max(0,-n+k1+k2+l+ ceil(t/2)), min(2*m1, floor(t/2))+1):   
                gc=2*(n-k1)^2*(n+1)
                st=(2*l+2*k2)*(intermediatelee_Z4(m1,v)+intermediatelee_Z4(m2,v)+binomial(2*m2,v))
                nc=((binomial(2*m1,v)*binomial(2*m2,v))/(4^(k2+l)))*min(t-2*v+1, n-k1-k2-l)*2*min(2*v,k1+k2)
                ops= gc+st+nc
                prob=binomial(2*n,t)/(binomial(2*m1,v)*binomial(2*m2,v)*binomial(2*(n-k1-k2-l), t-2*v))
                cost=N(log(prob*ops,2))
                if cost<mincost:
                    mincost=copy(cost)
                    bestv=v
                    bestl=l
        cost=copy(mincost)
        v=bestv
        l=bestl
    endcost=copy(cost)
    KS=k1*k2+(2*k1+k2)*(n-k1-k2)
    print 'Given n= ', n, 'k1=', k1,'k2=', k2, 't=',t, 'v= ',v,'l=',l, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
  
  
   



############################  ISD over Z_p^m in the Lee metric ############################ 


### k= k1 +k2/m
    


#  average lee weight of a randomly chosen vector 
def Leeaverage(p,m):
    if p==2:
        mu= (p^m)/4
    else:
        mu=((p^m)^2-1)/(4*p^m)
    return N(mu)

# number of compositions up to M
def compositionupto(w,s,M):
    if w>s*M:
        comp=0
    elif s>w:
        comp=0
    elif s<=w:
        comp=0
        for j in range(0, min(s,floor((w-s)/M))+1):
            comp=comp+(-1)^j*binomial(s,j)*binomial(w-j*M-1,s-1)
    return comp
   
   
# number of vectors of fixed Lee weight and fixed support size  
def countgivensupp(n,s,w,q):
    M=floor(q/2)
    if q.mod(2)==0:
        if n==0:
            c=0
        elif s>w:
            c=0
        elif w>s*M:
            c=0
        elif w==s*M:
            c=binomial(n,s)
        elif w<s*M:
            c=0
            for k in range(0, min(s, 2*w/q)+1):
                c=c+binomial(n-k,s-k)*2^(s-k)*binomial(n,k)*compositionupto(w-k*M,s-k,M-1)
    if q.mod(2)==1:
        if n==0:
            c=0
        elif s>w:
            c=0
        elif w>s*M:
            c=0
        elif w<=s*M:
            c=binomial(n,s)*2^s*compositionupto(w,s,M)
    return c
 
#  number of vectors of fixed Lee weight
def countLee(n,w,q):
    M=floor(q/2)
    if w==0 or n==0:
       c=1
    else:
        c=0
        for s in range(ceil(w/M),min(n,w)+1):
            c=c+countgivensupp(n,s,w,q)
    return c  
    
#  number of vectors of fixed Lee weight
def countLee_copy(n,w,q):
    M=floor(q/2)
    if w==0 or n==0:
        c=1
    else:
        c=0
        if q.mod(2)==0:
            for s in range(ceil(w/M),min(n,w)+1):
                if s==w/M:
                    c = c+ binomial(n,s)
                else:
                    for k in range(0, min(s, 2*w/q)+1):
                        c=c+binomial(n-k,s-k)*2^(s-k)*binomial(n,k)*compositionupto(w-k*M,s-k,M-1)
        else:
            for s in range(ceil(w/M),min(n,w)+1):
                c = c + binomial(n,s)*2^s*compositionupto(w,s,M)
    return c 
    
# GV bound in Lee metric

def GV_Lee_wantd(n,k,p,m):
    if p==2:
        d=2
        c=countLee(n,d-1,p^m)
        while (p^m)^k < (p^m)^n/(((c-1)/2+1)*(p^m-1)):
            d=d+1
            c=countLee(n,d-1,p^m)
    else:
        d=2
        c=countLee(n,d-1,p^m)
        while (p^m)^k < (p^m)^n/(( c-1)*(p^m-1)):
            d=d+1
            c=countLee(n,d-1,p^m)
    return d            

  


# cost of Pranges algorithm over Z_(p^m)
def workfactor_prange_Z(n, K, k1, t, p,m):
    gc=(n-k1)^2*(n+1)*(log(p^m,2)^2+log(p^m,2))
    prob=countLee(n,t,p^m)*countLee(n-K,t,p^m)^(-1)
    cost=N(log(prob*gc,2))
    KS=N(log(p^m,2)*K*(n-K)+log(p^(m-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,  't=',t,  'KS=', KS
    print 'cost= 2^',cost, 'bit ops'

# cost of Pranges algorithm over Z_(p^m), fixed rate R, fixed security level,  and assuming the GV bound, here we fix a small choice of k1   
def workfactor_prange_Z_fixedsec_small(n, R, p,m,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_Lee_wantd(n,k,p,m)
        t=floor((d-1)/2)
        k1=1
        k2=(k-k1)*m
        K=k1+k2
        while countLee(n-K,t,p^m)== 0:
            k1=k1+1
            k2=(k-k1)*m
            K=k1+k2 
        gc=(n-k1)^2*(n+1)*(log(p^m,2)^2+log(p^m,2))
        prob=countLee(n,t,p^m)*countLee(n-K,t,p^m)^(-1)
        cost=N(log(prob*gc,2))
    endcost=copy(cost)
    KS=N(log(p^m,2)*K*(n-K)+log(p^(m-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'k2=', k2, 'k=', k, 'K=', K, 'd=', d, 't=',t,  'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
    
    
    
# cost of Pranges algorithm over Z_(p^m), fixed rate R, fixed security level, and assuming the GV bound, here we fix a big choice of k1   
def workfactor_prange_Z_fixedsec_big(n, R, p,m,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_Lee_wantd(n,k,p,m)
        t=floor((d-1)/2)
        k1=k-1
        k2=(k-k1)*m
        K=k1+k2
        while countLee(n-K,t,p^m)== 0:
            k1=k1-1
            k2=(k-k1)*m
            K=k1+k2 
        gc=(n-k1)^2*(n+1)*(log(p^m,2)^2+log(p^m,2))
        prob=countLee(n,t,p^m)*countLee(n-K,t,p^m)^(-1)
        cost=N(log(prob*gc,2))
    endcost=copy(cost)
    KS=N(log(p^m,2)*K*(n-K)+log(p^(m-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1,'k2=', k2, 'k=', k,'K=', K, 'd=', d, 't=',t,  'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
    

  
 

# cost of Lee-Brickell s algorithm over Z_(p^m)   
def workfactor_LeeBrickell_Z(n, K, k1, t,p,m):
    mincost=100000000
    for v in range(1,min(t, floor((p^m)/2)*K)):
        sp=countLee(K,v,p^m)^(-1)*countLee(n-K,t-v,p^m)^(-1)*countLee(n,t,p^m)
        gc=(n-k1)^2*(n+1)*(log(p^m,2)^2+log(p^m,2))
        it=countLee(K,v,p^m)*(min(v,K)*(K-k1)+min(Leeaverage(p,m)^(-1)*(t-v+1),n-K)*min(v,K))*(log(p^m,2)^2+log(p^m,2))
        ops=gc+it
        cost=N(log(sp*ops,2))
        if cost<mincost:
            mincost=copy(cost)
            bestv=v
    KS=N(log(p^m,2)*K*(n-K)+log(p^(m-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,  't=',t, 'v= ',v, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
    
# cost of Lee-Brickell s algorithm over Z_(p^m), fixed rate R, fixed security level, and assuming the GV bound, here we fix a small choice of k1   
def workfactor_LeeBrickell_Z_fixedsec_small(n, R,p,m, seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_Lee_wantd(n,k,p,m)
        t=floor((d-1)/2)
        k1=1
        k2=(k-k1)*m
        K=k1+k2
        while countLee(n-K,t,p^m)== 0:
            k1=k1+1
            k2=(k-k1)*m
            K=k1+k2 
        mincost=100000000
        for v in range(1,min(t, floor((p^m)/2)*K)):
            sp=countLee(K,v,p^m)^(-1)*countLee(n-K,t-v,p^m)^(-1)*countLee(n,t,p^m)
            gc=(n-k1)^2*(n+1)*(log(p^m,2)^2+log(p^m,2))
            it=countLee(K,v,p^m)*(min(v,K)*(K-k1)+min(Leeaverage(p,m)^(-1)*(t-v+1),n-K)*min(v,K))*(log(p^m,2)^2+log(p^m,2))
            ops=gc+it
            cost=N(log(sp*ops,2))
            if cost<mincost:
                mincost=copy(cost)
                bestv=v
        cost=copy(mincost)
    endcost=copy(cost)
    KS=N(log(p^m,2)*K*(n-K)+log(p^(m-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1,'k2=', k2, 'k=', k,'K=', K, 'd=', d, 't=',t, 'v= ',v, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'   
    
    
# cost of Lee-Brickell s algorithm over Z_(p^m), fixed rate R, fixed security level,   and assuming the GV bound, here we fix a big choice of k1   
def workfactor_LeeBrickell_Z_fixedsec_big(n, R,p,m, seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_Lee_wantd(n,k,p,m)
        t=floor((d-1)/2)
        k1=k-1
        k2=(k-k1)*m
        K=k1+k2
        while countLee(n-K,t,p^m)== 0:
            k1=k1-1
            k2=(k-k1)*m
            K=k1+k2 
        mincost=100000000
        for v in range(1,min(t, floor((p^m)/2)*K)):
            sp=countLee(K,v,p^m)^(-1)*countLee(n-K,t-v,p^m)^(-1)*countLee(n,t,p^m)
            gc=(n-k1)^2*(n+1)*(log(p^m,2)^2+log(p^m,2))
            it=countLee(K,v,p^m)*(min(v,K)*(K-k1)+min(Leeaverage(p,m)^(-1)*(t-v+1),n-K)*min(v,K))*(log(p^m,2)^2+log(p^m,2))
            ops=gc+it
            cost=N(log(sp*ops,2))
            if cost<mincost:
                mincost=copy(cost)
                bestv=v
        cost=copy(mincost)
    endcost=copy(cost)
    KS=N(log(p^m,2)*K*(n-K)+log(p^(m-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1,'k2=', k2, 'k=', k,'K=', K, 'd=', d, 't=',t, 'v= ',v, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
    
    
   

#  cost of Sterns algorithm over Z_(p^m)
def workfactor_stern_Z(n,K,k1, t, p, m):
    mincost=1000000000000
    m1=floor(K/2)
    m2=K-m1
    M= floor((p^m)/2)
    for v in range(0, min(m1*M, floor(t/2)-1)+1):
        for l in range(0, max(n-K-ceil((t+2*v)/M)+1,0)):
            gc= (n-k1)^2*(n+1)*(log(p^m,2)^2+log(p^m,2))
            cs=  countLee(m1,v,p^m)*(l+K-k1)*min(v,m1)*(log(p^m,2)^2 +log(p^m,2))
            ct= countLee(m2,v,p^m)*(K-k1+l)*min(v,m2)*(log(p^m,2)^2+log(p^m,2))
            coll= countLee(m1,v,p^m)*countLee(m2,v,p^m)*(p^(-m*(l+K-k1)))*min(Leeaverage(p,m)^(-1)*(t-2*v+1),n-K-l)*min(2*v,K)*(log(p^m,2)+log(p^m,2)^2)
            ops=gc+cs+ct+coll
            prob=countLee(m1,v,p^m)^(-1)*countLee(m2,v,p^m)^(-1)*countLee(n-K-l,t-2*v,p^m)^(-1)*countLee(n,t,p^m)
            cost=N(log(prob*ops,2))
            if cost<mincost:
                bestv=v
                bestl=l
                mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    KS=N(log(p^m,2)*K*(n-K)+log(p^(m-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v,'l=',l, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
    
    
#  cost of Sterns algorithm over Z_(p^m), fixed rate R, fixed security level,  and assuming the GV bound, here we fix a small choice of k1   
def workfactor_stern_Z_fixedsec_small(n,R, p, m,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_Lee_wantd(n,k,p,m)
        t=floor((d-1)/2)
        k1=1
        k2=(k-k1)*m
        K=k1+k2
        while countLee(n-K,t,p^m)== 0:
            k1=k1+1
            k2=(k-k1)*m
            K=k1+k2 
        mincost=1000000000000
        m1=floor(K/2)
        m2=K-m1
        M= floor((p^m)/2)
        for v in range(0, min(m1*M, floor(t/2)-1)+1):
            for l in range(0, max(n-K-ceil((t+2*v)/M)+1,0)):
                gc= (n-k1)^2*(n+1)*(log(p^m,2)^2+log(p^m,2))
                cs=  countLee(m1,v,p^m)*(l+K-k1)*min(v,m1)*(log(p^m,2)^2 +log(p^m,2))
                ct= countLee(m2,v,p^m)*(K-k1+l)*min(v,m2)*(log(p^m,2)^2+log(p^m,2))
                coll= countLee(m1,v,p^m)*countLee(m2,v,p^m)*(p^(-m*(l+K-k1)))*min(Leeaverage(p,m)^(-1)*(t-2*v+1),n-K-l)*min(2*v,K)*(log(p^m,2)+log(p^m,2)^2)
                ops=gc+cs+ct+coll
                prob=countLee(m1,v,p^m)^(-1)*countLee(m2,v,p^m)^(-1)*countLee(n-K-l,t-2*v,p^m)^(-1)*countLee(n,t,p^m)
                cost=N(log(prob*ops,2))
                if cost<mincost:
                    bestv=v
                    bestl=l
                    mincost=copy(cost)
        cost=copy(mincost)
        v=bestv
        l=bestl
    endcost=copy(cost)
    KS=N(log(p^m,2)*K*(n-K)+log(p^(m-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1,'k2=', k2, 'k=', k,'K=', K, 'd=', d, 't=',t, 'v= ',v,'l=',l, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'  
    
    
    
#  cost of Sterns algorithm over Z_(p^m), fixed rate R, fixed security level,   and assuming the GV bound, here we fix a big choice of k1   
def workfactor_stern_Z_fixedsec_big(n,R, p, m,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_Lee_wantd(n,k,p,m)
        t=floor((d-1)/2)
        k1=k-1
        k2=(k-k1)*m
        K=k1+k2
        while countLee(n-K,t,p^m)== 0:
            k1=k1-1
            k2=(k-k1)*m
            K=k1+k2 
        mincost=1000000000000
        m1=floor(K/2)
        m2=K-m1
        M= floor((p^m)/2)
        for v in range(0, min(m1*M, floor(t/2)-1)+1):
            for l in range(0, max(n-K-ceil((t+2*v)/M)+1,0)):
                gc= (n-k1)^2*(n+1)*(log(p^m,2)^2+log(p^m,2))
                cs=  countLee(m1,v,p^m)*(l+K-k1)*min(v,m1)*(log(p^m,2)^2 +log(p^m,2))
                ct= countLee(m2,v,p^m)*(K-k1+l)*min(v,m2)*(log(p^m,2)^2+log(p^m,2))
                coll= countLee(m1,v,p^m)*countLee(m2,v,p^m)*(p^(-m*(l+K-k1)))*min(Leeaverage(p,m)^(-1)*(t-2*v+1),n-K-l)*min(2*v,K)*(log(p^m,2)+log(p^m,2)^2)
                ops=gc+cs+ct+coll
                prob=countLee(m1,v,p^m)^(-1)*countLee(m2,v,p^m)^(-1)*countLee(n-K-l,t-2*v,p^m)^(-1)*countLee(n,t,p^m)
                cost=N(log(prob*ops,2))
                if cost<mincost:
                    bestv=v
                    bestl=l
                    mincost=copy(cost)
        cost=copy(mincost)
        v=bestv
        l=bestl
    endcost=copy(cost)
    KS=N(log(p^m,2)*K*(n-K)+log(p^(m-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1,'k2=', k2, 'k=', k,'K=', K, 'd=', d, 't=',t, 'v= ',v,'l=',l, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
    
    
    



############################  ISD over F_p in the Lee metric ############################ 
 

 # sum all vectors for Lee sphere

def Lee_sphere(p,m,n,w):
    c=0
    q=p^m
    for i in range(0,w+1):
        c=c+countLee(n,i,q)
    return c
    
    
# GV bound for Lee metric over F_p
def GV_p_Z_wantd(n,k,p):
    i=1
    c=Lee_sphere(p,1,n,i-1)
    while (c-1)/2<(p^(n-k+1)-1)/(p-1):
        i=i+1
        c=Lee_sphere(p,1,n,i-1)
    return i-1 
 
 
#this computes the workfactor of Prange over F_p
def workfactor_prange_LEE_Fp(n,K,t,p):
    gc=(n-K)^2*(n+1)*(log(p,2)^2+log(p,2))
    prob=countLee(n,t,p)*countLee(n-K,t,p)^(-1)   
    cost=N(log(prob*gc,2))
    KS=N(K*(n-K)*log(p,2))
    print 'Given n= ', n, 'K=', K, 't=',t,  'KS=', KS
    print 'cost= 2^',cost, 'bit ops'  
    
    

        
# workfactor of Prange over F_p, fixed rate R, fixed security level,   and assuming the GV bound
def workfactor_prange_LEE_Fp_fixedsec(n,R,p,seclevel):    
    cost=0
    while cost<seclevel:
    	n=n+1
        K=floor(R*n)
        d=GV_p_Z_wantd(n,K,p)
        t=floor((d-1)/2)
        gc=(n-K)^2*(n+1)*(log(p,2)^2+log(p,2))
        prob=countLee(n,t,p)*countLee(n-K,t,p)^(-1)   
        cost=N(log(prob*gc,2))
    endcost=copy(cost)
    KS=N(K*(n-K)*log(p,2))
    print 'Given n= ', n, 'K=', K, 't=',t, 'd=', d,  'KS=', KS
    print 'cost= 2^',endcost, 'bit ops' 
    
    


    
# this computes the workfactor of Lee-Brickell over F_p   in Lee metric
def workfactor_LeeBrickell_LEE_Fp(n,K,t,p):
    mincost=100000000000
    for v in range(1, min(t,floor(p/2)*K)+1):
        sp=countLee(K,v,p)^(-1)*countLee(n-K,t-v,p)^(-1)*countLee(n,t,p)
        gc=(n-K)^2*(n+1)*(log(p,2)^2+log(p,2))
        it=countLee(K,v,p)*min(Leeaverage(p,1)^(-1)*(t-v+1),n-K)*min(v,K)*(log(p,2)^2+log(p,2))
        ops=gc+it
        cost=N(log(sp*ops,2))
        if cost<mincost:
            mincost=copy(cost)
            bestv=v
    KS=N(log(p,2)*K*(n-K))
    print 'Given n= ', n, 'K=', K, 't=',t,  'v=', v,   'KS=', KS
    print 'cost= 2^',mincost, 'bit ops'


    
# this computes the workfactor of Lee-Brickell over F_p   in Lee metric, fixed rate R, fixed security level, seclevel, and assuming the GV bound
def workfactor_LeeBrickell_LEE_Fp_fixedsec(n,R,p,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        K=floor(R*n)
        d=GV_p_Z_wantd(n,K,p)
        t=floor((d-1)/2)
        mincost=100000000000
        for v in range(1, min(t,floor(p/2)*K)+1):
            sp=countLee(K,v,p)^(-1)*countLee(n-K,t-v,p)^(-1)*countLee(n,t,p)
            gc=(n-K)^2*(n+1)*(log(p,2)^2+log(p,2))
            it=countLee(K,v,p)*min(Leeaverage(p,1)^(-1)*(t-v+1),n-K)*min(v,K)*(log(p,2)^2+log(p,2))
            ops=gc+it
            cost=N(log(sp*ops,2))
            if cost<mincost:
                mincost=copy(cost)
                bestv=v
        cost=copy(mincost)
    endcost=copy(cost)
    KS=N(log(p,2)*K*(n-K))
    print 'Given n= ', n, 'K=', K, 't=',t, 'd=', d, 'v=', v,   'KS=', KS
    print 'cost= 2^',endcost, 'bit ops'


    
    
     

#  cost of Sterns algorithm over Z_p
def workfactor_stern_LEE_Fp(n,K, t,p):
    mincost=1000000000000
    m1=floor(K/2)
    m2=K-m1
    M=floor(p/2)
    for v in range(0, min(m1*M, m2*M, floor(t/2))+1):
        for l in range(0, max(n-K- ceil((t-2*v)/M),0)):
            gc= (n-K)^2*(n+1)*(log(p,2)^2+log(p,2))
            cs=  countLee(m1,v,p)*l*min(m1,v)*(log(p,2)^2+log(p,2))
            ct= countLee(m2,v,p)*l*min(m2,v)*(log(p,2)^2+log(p,2))
            coll= countLee(m1,v,p)*countLee(m2,v,p)*(p^(-l))*min(Leeaverage(p,1)^(-1)*(t-2*v+1),n-K-l)*min(K,2*v)*(log(p,2)+log(p,2)^2)
            ops=gc+cs+ct+coll
            prob=max(countLee(m1,v,p),1)^(-1)*max(countLee(m2,v,p),1)^(-1)*max(countLee(n-K-l,t-2*v,p),1)^(-1)*countLee(n,t,p)
            accost=N(log(prob*ops,2))
            if accost<mincost:
                bestv=v
                bestl=l
                mincost=copy(accost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    KS=N(log(p,2)*K*(n-K))
    print 'Given n= ', n, 'K=', K, 't=',t,   'v=', v, 'l=', l, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'


#  cost of Sterns algorithm over Z_p in Lee metric, fixed rate R, fixed security level, seclevel, and assuming the GV bound
def workfactor_stern_LEE_Fp_fixedsec(n,R,p,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        K=floor(R*n)
        d=GV_p_Z_wantd(n,K,p)
        t=floor((d-1)/2)
        mincost=1000000000000
        m1=floor(K/2)
        m2=K-m1
        M=floor(p/2)
        for v in range(0, min(m1*M, m2*M, floor(t/2))+1):
            for l in range(0, max(n-K- ceil((t-2*v)/M),0)):
                gc= (n-K)^2*(n+1)*(log(p,2)^2+log(p,2))
                cs=  countLee(m1,v,p)*l*min(m1,v)*(log(p,2)^2+log(p,2))
                ct= countLee(m2,v,p)*l*min(m2,v)*(log(p,2)^2+log(p,2))
                coll= countLee(m1,v,p)*countLee(m2,v,p)*(p^(-l))*min(Leeaverage(p,1)^(-1)*(t-2*v+1),n-K-l)*min(K,2*v)*(log(p,2)+log(p,2)^2)
                ops=gc+cs+ct+coll
                prob=max(countLee(m1,v,p),1)^(-1)*max(countLee(m2,v,p),1)^(-1)*max(countLee(n-K-l,t-2*v,p),1)^(-1)*countLee(n,t,p)
                accost=N(log(prob*ops,2))
                if accost<mincost:
                    bestv=v
                    bestl=l
                    mincost=copy(accost)
        cost=copy(mincost)
        v=bestv
        l=bestl
    endcost=copy(cost)
    KS=N(log(p,2)*K*(n-K))
    print 'Given n= ', n, 'K=', K, 't=',t, 'd=', d, 'v=', v, 'l=', l, 'KS=', KS
    print 'cost= 2^',endcost, 'bit ops'

#########################################################################
############################  New algorithms ############################ 
#########################################################################


# cost of doing intermediate sums over Z_p^s:
# cost of computing xA, where A is k x n matrix over Z_p^s, for all x having Lee weight t
def intermediatelee_Zps(n,k,t,p,s):
    return countLee_copy(k,t,p^s)*n*floor(log(p^s,2))



# cost of 2-blocks algorithm over Z_(p^m)   
# Input: n, K = k1 + k2 + ... + ks, k1, t, p, m
def workfactor_two_blocks_Zpm(n,K,k1, t, p, m):
    mincost=1000000000000
    m1=floor(K/2)
    m2=K-m1
    M= floor((p^m)/2)
    gc= (n-k1)^2*(n+1)*(log(p^m,2)^2+log(p^m,2))
    cn = countLee(n,t,p^m)
    for v in range(0, min(m1*M, floor(t/2)-1)+1):
        print v
        cntl1 = countLee(m1,v,p^m)
        cntl2 = countLee(m2,v,p^m)
        for l in range(0, max(n-K-ceil((t-2*v)/M)+1,0)):
            #cs= intermediatelee_Zps(l,m1,v,p,m) + intermediatelee_Zps(K-k1,m1,v,p,m)
            #ct= countLee(m2,v,p^m)*(K-k1+l)*log(p^m,2) + intermediatelee_Zps(l,m2,v,p,m) + intermediatelee_Zps(K-k1,m2,v,p,m)
            #coll= countLee(m1,v,p^m)*countLee(m2,v,p^m)*(p^(-m*(l+K-k1)))*min(Leeaverage(p,m)^(-1)*(t-2*v+1),n-K-l)*min(2*v,K)*(log(p^m,2)+log(p^m,2)^2)
            ops=gc + cntl1*(l+K-k1)*log(p^m,2) + 2*cntl2*(K-k1+l)*log(p^m,2) + cntl1*cntl2*(p^(-m*(l+K-k1)))*min(Leeaverage(p,m)^(-1)*(t-2*v+1),n-K-l)*min(2*v,K)*(log(p^m,2)+log(p^m,2)^2)
            prob=cntl1^(-1)*cntl2^(-1)*countLee(n-K-l,t-2*v,p^m)^(-1)*cn
            cost=N(log(prob*ops,2))
            if cost<mincost:
                bestv=v
                bestl=l
                mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    KS=N(log(p^m,2)*K*(n-K)+log(p^(m-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v,'l=',l, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
    





# cost of 2-blocks algorithm over Z_(p^m)   
# Input: n, K = k1 + k2 + ... + ks, k1, t, p, m
def workfactor_two_blocks_Zpm_nol(n,K,k1, t, p, m):
    mincost=1000000000000
    m1=floor(K/2)
    m2=K-m1
    l=0
    M= floor((p^m)/2)
    gc= (n-k1)^2*(n+1)*(log(p^m,2)^2+log(p^m,2))
    cn = countLee(n,t,p^m)
    for v in range(0, min(m1*M, floor(t/2)-1)+1):
        print v
        cntl1 = countLee(m1,v,p^m)
        cntl2 = countLee(m2,v,p^m)
        #cs= intermediatelee_Zps(l,m1,v,p,m) + intermediatelee_Zps(K-k1,m1,v,p,m)
        #ct= countLee(m2,v,p^m)*(K-k1+l)*log(p^m,2) + intermediatelee_Zps(l,m2,v,p,m) + intermediatelee_Zps(K-k1,m2,v,p,m)
        #coll= countLee(m1,v,p^m)*countLee(m2,v,p^m)*(p^(-m*(l+K-k1)))*min(Leeaverage(p,m)^(-1)*(t-2*v+1),n-K-l)*min(2*v,K)*(log(p^m,2)+log(p^m,2)^2)
        ops=gc + cntl1*(K-k1)*log(p^m,2) + 2*cntl2*(K-k1)*log(p^m,2) + cntl1*cntl2*(p^(-m*(l+K-k1)))*min(Leeaverage(p,m)^(-1)*(t-2*v+1),n-K-l)*min(2*v,K)*(log(p^m,2)+log(p^m,2)^2)
        prob=cntl1^(-1)*cntl2^(-1)*countLee(n-K-l,t-2*v,p^m)^(-1)*cn
        cost=N(log(prob*ops,2))
        if cost<mincost:
            bestv=v
            mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    KS=N(log(p^m,2)*K*(n-K)+log(p^(m-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v,  'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
    


   
# cost of 2-blocks algorithm over Z_(p^m)  with choosable m1,m2,v1,v2, gives a minimal speed up we can also forget about it
def workfactor_two_blocks_choose_Zpm(n,K,k1, t, p, m):
    mincost=1000000000000
    M= floor((p^m)/2)
    gc= (n-k1)^2*(n+1)*(log(p^m,2)^2+log(p^m,2))
    for m1 in range(0,K+1):
        m2=K-m1
        for v1 in range(0, min(m1*M, t)+1):
            for v2 in range(0, min(m2*M, t-v1)+1):
                for l in range(0, max(n-K-ceil((t+v1+v2)/M)+1,0)):
                    cs= intermediatelee_Zps(l,m1,v1,p,m) + intermediatelee_Zps(K-k1,m1,v1,p,m)
                    ct= countLee(m2,v2,p^m)*(K-k1+l)*log(p^m,2) + intermediatelee_Zps(l,m2,v2,p,m) + intermediatelee_Zps(K-k1,m2,v2,p,m)
                    coll= countLee(m1,v1,p^m)*countLee(m2,v2,p^m)*(p^(-m*(l+K-k1)))*min(Leeaverage(p,m)^(-1)*(t-v1-v2+1),n-K-l)*min(v1+v2,K)*(log(p^m,2)+log(p^m,2)^2)
                    ops=gc+cs+ct+coll
                    prob=countLee(m1,v1,p^m)^(-1)*countLee(m2,v2,p^m)^(-1)*countLee(n-K-l,t-v1-v2,p^m)^(-1)*countLee(n,t,p^m)
                    cost=N(log(prob*ops,2))
                    if cost<mincost:
                        bestm1=m1
                        bestm2=m2
                        bestv1=v1
                        bestv2=v2
                        bestl=l
                        mincost=copy(cost)
    cost=copy(mincost)
    v1=bestv1
    v2=bestv2
    m1=bestm1
    m2=bestm2
    l=bestl
    KS=N(log(p^m,2)*K*(n-K)+log(p^(m-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v1= ',v1, 'v2= ',v2, 'm1= ',m1, 'm2= ',m2,'l=',l, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'   

    
# cost of 2-blocks algorithm over Z_(p^m)  with fixed security level and fixed rate assuming GV optimizing also K
def workfactor_two_blocks_fixedsec_optK(n,R, p, m, seclevel):
    cost=0
    while cost<seclevel:
    	n=n+100
        k=floor(R*n)
        d=GV_Lee_wantd(n,k,p,m)
        t=floor((d-1)/2)
        mincost=1000000000000
        M= floor((p^m)/2)
        for K in range(k,n+1):
            for k1 in range(0,k+1):
                print n,K,k1,cost
                m1=floor(K/2)
                m2=K-m1
                gc= (n-k1)^2*(n+1)*(log(p^m,2)^2+log(p^m,2))
                for v in range(0, min(m1*M, floor(t/2)-1)+1):
                    for l in range(0, max(n-K-ceil((t+2*v)/M)+1,0)):
                        cs= intermediatelee_Zps(l,m1,v,p,m) + intermediatelee_Zps(K-k1,m1,v,p,m)
                        ct= countLee(m2,v,p^m)*(K-k1+l)*log(p^m,2) + intermediatelee_Zps(l,m2,v,p,m) + intermediatelee_Zps(K-k1,m2,v,p,m)
                        coll= countLee(m1,v,p^m)*countLee(m2,v,p^m)*(p^(-m*(l+K-k1)))*min(Leeaverage(p,m)^(-1)*(t-2*v+1),n-K-l)*min(2*v,K)*(log(p^m,2)+log(p^m,2)^2)
                        ops=gc+cs+ct+coll
                        prob=countLee(m1,v,p^m)^(-1)*countLee(m2,v,p^m)^(-1)*countLee(n-K-l,t-2*v,p^m)^(-1)*countLee(n,t,p^m)
                        cost=N(log(prob*ops,2))
                        if cost<mincost:
                            bestv=v
                            bestl=l
                            bestk1=k1
                            bestK=K
                            mincost=copy(cost)
        cost=copy(mincost)
        v=bestv
        l=bestl
        K=bestK
        k1=bestk1
    KS=N(log(p^m,2)*K*(n-K)+log(p^(m-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k=',k, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v,'l=',l, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops' 
    



# cost of 2-blocks algorithm over Z_(p^m)  with fixed security level and fixed rate assuming GV worst K
def workfactor_two_blocks_fixedsec_morerates(n,R, RI,R1, p, m, seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
    	bestv=0
    	bestl=0
        k=floor(R*n)
        K=floor(RI*n)
        k1=floor(R1*n)
        d=GV_Lee_wantd(n,k,p,m)
        t=floor((d-1)/2)
        mincost=1000000000000
        M= floor((p^m)/2)    
        m1=floor(K/2)
        m2=K-m1
        gc= (n-k1)^2*(n+1)*(log(p^m,2)^2+log(p^m,2))
        for v in range(0, min(m1*M, floor(t/2))+1):
            for l in range(0, n-K-ceil((t-2*v)/M)+1):
                cs= intermediatelee_Zps(l,m1,v,p,m) + intermediatelee_Zps(K-k1,m1,v,p,m)
                ct= countLee(m2,v,p^m)*(K-k1+l)*log(p^m,2) + intermediatelee_Zps(l,m2,v,p,m) + intermediatelee_Zps(K-k1,m2,v,p,m)
                coll= countLee(m1,v,p^m)*countLee(m2,v,p^m)*(p^(-m*(l+K-k1)))*min(Leeaverage(p,m)^(-1)*(t-2*v+1),n-K-l)*min(2*v,K)*(log(p^m,2)+log(p^m,2)^2)
                ops=gc+cs+ct+coll
                prob=countLee(m1,v,p^m)^(-1)*countLee(m2,v,p^m)^(-1)*countLee(n-K-l,t-2*v,p^m)^(-1)*countLee(n,t,p^m)
                cost=N(log(prob*ops,2))
                if cost<mincost:
                    bestv=v
                    bestl=l
                    mincost=copy(cost)
        cost=copy(mincost)
        v=bestv
        l=bestl
    KS=N(log(p^m,2)*K*(n-K)+log(p^(m-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k=',k, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v,'l=',l, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'        
 
 
#count size of Dumer's set
def count_Dumer(l,n,t,p,m):
    c=0
    M= floor((p^m)/2)
    for v in range(0, l*M+1):
        if countLee(l,v,p^m) <= countLee(n-l,t-v,p^m):
            a=0
            for w in range(0, (l+1)*M+1):
                if countLee(l+1,w,p^m)> countLee(n-l-1,t-w,p^m):
                    a=1
                    break
            if a==1:
                c=c+1
    return c
    
    
# set of Dumer
def set_Dumer(l,n,t,p,m):
    L=[]
    c = 0
    M= floor((p^m)/2)
    for v in range(0, min(t,l*M)+1):
        if (countLee(l,v,p^m) <= countLee(n-l,t-v,p^m))&(countLee(l+1,min(v+M,t),p^m) > countLee(n-l-1,t-min(v+M,t),p^m)):
            c = c +1
            L.append(v)
            #a=0
            #for w in range(v, min(v+M,t)+1):
            #    if countLee(l+1,w,p^m)> countLee(n-l-1,t-w,p^m):
            #        a=1
            #        break
            #if a==1:
            #    L.append(v)
            #    c1 = c1+1
    return L    
 
# cost of Dumer    
def Dumer_Zpm(n,K,k1,t,p,m):
    M= floor((p^m)/2)
    c=0
    for l in range(1,n):
        print(l)
        for v in set_Dumer(l,n,t,p,m):
            L1=countLee(l,v,p^m)
            L2=countLee(n-l,t-v,p^m)
            cost=intermediatelee_Zps(n-k1,l,v,p,m)+L1*ceil(log(L1,2))+intermediatelee_Zps(n-k1,n-l,t-v,p,m)+L2*(n-k1)*ceil(log(p^m,2))+L2*ceil(log(L2,2))
            c=c+cost
    cost= N(log(c,2))
    KS=N(log(p^m,2)*K*(n-K)+log(p^(m-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t,  'KS=', KS
    print 'cost= 2^',cost, 'bit ops' 
    
    
# cost of Dumer   with fixed security level and fixed rate assuming GV 
def Dumer_Zpm_fixedsec_big(n,R,p,m,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        k=floor(R*n)
        d=GV_Lee_wantd(n,k,p,m)
        t=floor((d-1)/2)
        mincost=1000000000000
        M= floor((p^m)/2)
        for K in range(k,n+1):
            for k1 in range(0,K+1):
                print n,K,k1
                c=0
                for l in range(1,n):
                    for v in set_Dumer(l,n,t,p,m):
                        L1=countLee(l,v,p^m)
                        L2=countLee(n-l,t-v,p^m)
                        cost=intermediatelee_Zps(n-k1,l,v,p,m)+L1*ceil(log(L1,2))+intermediatelee_Zps(n-k1,n-l,t-v,p,m)+L2*(n-k1)*ceil(log(p^m,2))+L2*ceil(log(L2,2))
                        c=c+cost
                cost= N(log(c,2))
                if cost<mincost:
                    mincost=copy(cost)
                    bestK=K
                    bestk1=k1
        cost=copy(mincost)
        K=bestK
        k1=bestk1
    KS=N(log(p^m,2)*K*(n-K)+log(p^(m-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t,  'KS=', KS
    print 'cost= 2^',cost, 'bit ops'     
 
 
     
     
    
     
# Cost of nested loops section C(w1,...ws) of s-blocks algorithm over Z_(p^s)
# Input: k=[k1,...,ks+1], w = [w1,...,ws+1], p, s
def sblocks_C(k,w,p,s):
    c = 0
    for j in range(2,s+2):
        f = k[j-1]*k[0]*(log(p^(j-1),2)^2+log(p^(j-1),2))
        pg = 1
        for l in range(2,j):
            f = f + k[j-1]*k[l-1]*(log(p^(j-1),2)^2+log(p^(j-1),2))
            pg = pg*p^(k[l-1]*(s-l+1))*Leeaverage(p,s)^(-1)*(w[l-1]+1)
        c = c + f*pg
    return N(c)

# cost of s-blocks algorithm over Z_(p^s)    
# Input: n, k=[k1,k2,...,ks], t, p, s
def workfactor_s_blocks_Zpm(n,k,t, p, s):
    mincost=1000000000000
    K = sum(k)     #K= k1 + ... + ks
    M = floor(p^s/2)
    gc = (n-k[0])^2*(n+1)*(log(p^s,2)^2+log(p^s,2))
    for v in range(0,min(K*M,t)+1):
        print v
        num_iter = countLee(K,v,p^s)^(-1)*countLee(n-K,t-v,p^s)^(-1)*countLee(n,t,p^s) - 1
        s1 = 0
        for w in Compositions(v,length=s,min_part=0,outer=[M*j for j in k]):
            k.append(n-K)
            ww=list(w)
            ww.append(t-sum(ww))
            s1 = s1 + countLee(k[0],ww[0],p^s)*sblocks_C(k,ww,p,s)
        s2 = s1/(binomial(v+s-1,s-1))
        cost=N(log(num_iter*(gc+s1)+gc+s2,2))
        if cost<mincost:
            bestv=v
            mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k[1]))
    print 'Given n= ', n, 'k1=', k[0], 'K=', K,   't=',t, 'v= ',v, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
    
    
    
# cost of s-blocks second variant algorithm over Z_(p^s)    
# Input: n, k=[k1,k2,...,ks], t, p, s
def workfactor_s_blocks_second_variant(n,k,t, p, s):
    mincost=1000000000000
    K = sum(k)     #K= k1 + ... + ks
    M = floor(p^s/2)
    gc = (n-k[0])^2*(n+1)*(log(p^s,2)^2+log(p^s,2))
    for v1 in range(0,min(k[0]*M,t)+1):
        for v2 in range(max(0,t-v1-(n-K)*M), min(t-v1,k[1]*M)+1):
            num_iter = countLee(k[0],v1,p^s)^(-1)*countLee(k[1],v2,p^s)^(-1)*countLee(n-K,t-v1-v2,p^s)^(-1)*countLee(n,t,p^s)
            w=[]
            w.append(v1)
            w.append(v2)
            for j in range(0,s-1):
                w.append(0)
            print w
            k.append(n-K)
            c = countLee(k[0],v1,p^s)*sblocks_C(k,w,p,s)
            cost=N(log(num_iter*(gc+c),2))
            if cost<mincost:
                bestv1=v1
                bestv2=v2
                mincost=copy(cost)
    cost=copy(mincost)
    v1=bestv1
    v2=bestv2
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k[1]))
    print 'Given n= ', n, 'k1=', k[0], 'K=', K,   't=',t, 'v1= ',v1, 'v2=', v2, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'    
    
 
 
# cost of s-blocks algorithm over Z_(p^s) with fixed security level and rate, assuming GV  
def workfactor_s_blocks_Zpm_fixedsec(n,R, p, s,seclevel):
    cost=0
    while cost<seclevel:
    	n=n+1
        kk=floor(R*n)
        d=GV_Lee_wantd(n,kk,p,s)
        t=floor((d-1)/2)
        mincost=1000000000000
        M= floor((p^s)/2)
        k1=kk-1
        K=k1+s
        k=list()
        k.append(k1)
        for j in range(2,s):
            k.append(0)
        k.append(s)
        gc = (n-k[0])^2*(n+1)*(log(p^s,2)^2+log(p^s,2))
        for v in range(0,min(K*M,t)+1):
            num_iter = countLee(K,v,p^s)^(-1)*countLee(n-K,t-v,p^s)^(-1)*countLee(n,t,p^s) - 1
            s1 = 0
            for w in Compositions(v,length=s,min_part=0,outer=[M*j for j in k]):
                k.append(n-K)
                ww=list(w)
                ww.append(t-sum(ww))
                s1 = s1 + countLee(k[0],ww[0],p^s)*sblocks_C(k,ww,p,s)
            s2 = s1/(binomial(v+s-1,s-1))
            cost=N(log(num_iter*(gc+s1)+gc+s2,2))
            if cost<mincost:
                bestv=v
                mincost=copy(cost)
        cost=copy(mincost)
        v=bestv
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k[1]))
    print 'Given n= ', n, 'k1=', k[0], 'K=', K,   't=',t, 'v= ',v, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops' 
 
 
    
    
def wagner(a, u, k1, l, v, p, s):
    b=0
    u.append(l)
    L=[countLee_copy( floor((k1+l)/(2^a)), floor(v/(2^a)), p^s)]
    beta=[]
    for i in range(1,a+1):
        d=0
        for m in range(1,i):
            d=d+2^(i-m-1)*u[m-1]
        beta.append(u[i-1]+d)
        L.append((L[0]^(2^i))/((p^s)^beta[i-1]))
        b=b+2^(a-i+1)*(L[i-1]*u[i-1]*(k1+l)/(2^(a-i+1))*(log(p^s,2)+log(p^s,2)^2) +(L[i-1]+1)*log(L[i-1],2))
    return [b, L[a]] 
    
'''I was trying to make wagner algo faster, but it is not that fast but has uses less memory in case we need it
def wagner_copy(a, u, k1, l, v, p, s):
    b=0
    u.append(l)
    Lo = countLee( floor((k1+l)/(2^a)), floor(v/(2^a)), p^s)
    Li = Lo
    d=0
    for i in range(1,a+1):
        d=d+2^(-1)*u[i-1]
        beta = u[i-1]+d
        b=b+2^(a-i+1)*(Li*u[i-1]*(k1+l)/(2^(a-i+1))*(log(p^s,2)+log(p^s,2)^2) +(Li+1)*log(Li,2))
        Li = (Lo^(2^i))/((p^s)^beta)
    return [b, Li] '''
    
def workfactor_GBA_wagner(n,K,k1,t,a,p,s):
    mincost=10000000000
    M=floor((p^s)/2)
    for l in range(1,n-k1+1):
        print l
        for v in range(max(1, t- (n-k1-l)*M), min(t,(k1+l)*M)+1):
            sp= countLee_copy(n,t,p^s)*countLee_copy(k1+l,v,p^s)^(-1)*countLee_copy(n-k1-l,t-v,p^s)^(-1)            
            for u in UnorderedTuples(range(l), a-1).list():
                cs= wagner(a,u,k1,l,v,p,s)
                it=cs[0]+((n-k1-l)^2*(n+1)+ cs[1]*(n-k1)*min((n-k1-l), Leeaverage(p,s)^(-1)*(t-v+1)))*(log(p^s,2)+log(p^s,2)^2)
                cost=N(log(it*sp,2))
                if cost<mincost:
                    bestv=v
                    bestl=l
                    bestu=u
                    mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    u=bestu
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v, 'l=', l, 'u=', u, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'         
    
def workfactor_GBA_wagner_optimal_a(n,K,k1,t,A,p,s):
    mincost=10000000000
    M=floor((p^s)/2)
    for l in range(1,n-k1+1):
        print l
        for v in range(max(1, t- (n-k1-l)*M), min(t,(k1+l)*M)+1):
            sp= countLee(n,t,p^s)*countLee(k1+l,v,p^s)^(-1)*countLee(n-k1-l,t-v,p^s)^(-1)
            for a in range(1,A+1):
                for u in UnorderedTuples(range(l), a-1).list():
                    it=wagner(a,u,k1,l,v,p,s)[0]+((n-k1-l)^2*(n+1)+ wagner(a,u,k1,l,v,p,s)[1]*(n-k1)*min((n-k1-l), Leeaverage(p,s)^(-1)*(t-v+1)))*(log(p^s,2)+log(p^s,2)^2)
                    cost=N(log(it*sp,2))
                    if cost<mincost:
                        bestv=v
                        bestl=l
                        bestu=u
                        besta=a
                        mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    u=bestu
    a=besta
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v, 'l=', l, 'a=', a, 'u=', u, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'  
    
    
    
def rep_tech(a, u, e, k1, l, v, p, s):
    u.append(l)
    w=[]
    c=0
    g=0
    for b in range(1,a+1):
        g=g+floor(e[b-1]/(2^b))
    L=[countLee(k1+l,floor(v/(2^a)+g),p^s)/(p^s)]
    for i in range(1,a+1):
        d=0
        for b in range(1,a-i+1):
            d=d+floor(e[b-1]/(2^b))
        w.append(floor(v/(2^(a-i)))+d)
        L.append(countLee(k1+l, w[i-1],p^s)/(p^(s*u[i-1])))
        c=c+2^(a-i+1)*(L[i-1]*u[i-1]*(k1+l)*(floor(log(p^s,2))+floor(log(p^s,2))^2)+ L[i-1]*(2*log(L[i-1],2)-1) + (k1+l)*(L[i-1]^2/(p^(s*u[i-1])))*floor(log(p^s,2)))
    return [N(c),N(L[a])]
    
def rep_tech_copy(a, u, e, k1, l, v, p, s):
    u.append(l)
    w=[]
    c=0
    g= sum([floor(e[b-1]/(2^b)) for b in range(1,a+1)])
    #for b in range(1,a+1):
    #    g=g+floor(e[b-1]/(2^b))
    L=[countLee_copy(k1+l,floor(v/(2^a)+g),p^s)/(p^s)]
    d=g
    for i in range(1,a+1):
        d=d - floor(e[a-i]/(2^(a-i+1)))
        w.append(floor(v/(2^(a-i)))+d)
        c=c+2^(a-i+1)*(L[i-1]*u[i-1]*(k1+l)*(log(p^s,2)+log(p^s,2)^2)+ L[i-1]*(2*log(L[i-1],2)-1) + (k1+l)*(L[i-1]^2/(p^(s*u[i-1])))*log(p^s,2))
        L.append(countLee_copy(k1+l, w[i-1],p^s)/(p^(s*u[i-1])))
    return [N(c),N(L[a])]
    
def random_e(k1,l,v,a,p,s):
    M=floor((p^s)/2)
    e=[]
    for j in range(0,a):
        c=0
        for b in range(0,j):
            c=c+e[b]*2^(j-b)
        e.append(ZZ.random_element(2^(j+1)*(k1+l)*M-v-c))
    return e
     

def workfactor_GBA_rep_tech(n,K,k1,t,a,e,p,s):
    mincost=10000000000
    M=floor((p^s)/2)
    for l in range(1,n-k1+1):
        for v in range(max(1, t- (n-k1-l)*M), min(t,(k1+l)*M)+1):
            sp= countLee_copy(n,t,p^s)*countLee_copy(k1+l,v,p^s)^(-1)*countLee_copy(n-k1-l,t-v,p^s)^(-1)
            for u in UnorderedTuples(range(l), a-1).list():
                cs = rep_tech_copy(a,u,e,k1,l,v,p,s)
                it=cs[0]+((n-k1-l)^2*(n+1)+ cs[1]*(n-k1)*min((n-k1-l), Leeaverage(p,s)^(-1)*(t-v+1)))*(log(p^s,2)+log(p^s,2)^2)
                cost=N(log(it*sp,2))
                if cost<mincost:
                    bestv=v
                    bestl=l
                    bestu=u
                    mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    u=bestu
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v, 'l=', l, 'u=', u, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'     
    
    
import time    

def workfactor_GBA_rep_tech_random_e(n,K,k1,t,a,p,s):
    mincost=10000000000
    M=floor((p^s)/2)
    for l in range(1,n-k1+1):
        print l
        for v in range(max(1, t- (n-k1-l)*M), min(t,(k1+l)*M)+1):
            e=random_e(k1,l,v,a,p,s)
            sp= countLee(n,t,p^s)*countLee(k1+l,v,p^s)^(-1)*countLee(n-k1-l,t-v,p^s)^(-1)
            for u in UnorderedTuples(range(l), a-1).list():
                #start_time = time.time()
                cs = rep_tech_copy(a,u,e,k1,l,v,p,s)
                #print time.time() - start_time
                it=cs[0]+((n-k1-l)^2*(n+1)+ cs[1]*(n-k1)*min((n-k1-l), Leeaverage(p,s)^(-1)*(t-v+1)))*(log(p^s,2)+log(p^s,2)^2)
                cost=N(log(it*sp,2))
                if cost<mincost:
                    bestv=v
                    bestl=l
                    bestu=u
                    mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    u=bestu
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v, 'l=', l, 'u=', u, 'e=', e, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops' 

def workfactor_GBA_rep_tech_best_e_onelevel(n,K,k1,t,p,s):
    mincost=10000000000
    M=floor((p^s)/2)
    a=1
    for l in range(1,n-k1+1):
        print l
        for v in range(max(1, t- (n-k1-l)*M), min(t,(k1+l)*M)+1):
            sp= countLee_copy(n,t,p^s)*countLee_copy(k1+l,v,p^s)^(-1)*countLee_copy(n-k1-l,t-v,p^s)^(-1)
            for b in [0,2*(k1+l)*M-v]:
                e=[b]
                for u in UnorderedTuples(range(l), a-1).list():
                    cs = rep_tech_copy(a,u,e,k1,l,v,p,s)
                    it=cs[0]+((n-k1-l)^2*(n+1)+ cs[1]*(n-k1)*min((n-k1-l), Leeaverage(p,s)^(-1)*(t-v+1)))*(log(p^s,2)+log(p^s,2)^2)
                    cost=N(log(it*sp,2))
                    if cost<mincost:
                        bestv=v
                        bestl=l
                        bestu=u
                        beste=e
                        mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    u=bestu
    e=beste
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v, 'l=', l, 'u=', u, 'e=', e, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops' 
      

def workfactor_GBA_rep_tech_best_e_twolevel(n,K,k1,t,p,s):
    mincost=10000000000
    M=floor((p^s)/2)
    a=2
    for l in range(1,n-k1+1):
        print l
        for v in range(max(1, t- (n-k1-l)*M), min(t,(k1+l)*M)+1):
            sp= countLee_copy(n,t,p^s)*countLee_copy(k1+l,v,p^s)^(-1)*countLee_copy(n-k1-l,t-v,p^s)^(-1)
            for b in [0,2*(k1+l)*M-v]:
                for c in [0,4*(k1+l)*M-v-2*b]:
                    e=[b,c]
                    for u in UnorderedTuples(range(l), a-1).list():
                        cs = rep_tech_copy(a,u,e,k1,l,v,p,s)
                        it=cs[0]+((n-k1-l)^2*(n+1)+ cs[1]*(n-k1)*min((n-k1-l), Leeaverage(p,s)^(-1)*(t-v+1)))*(log(p^s,2)+log(p^s,2)^2)
                        cost=N(log(it*sp,2))
                        if cost<mincost:
                            bestv=v
                            bestl=l
                            bestu=u
                            beste=e
                            mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    u=bestu
    e=beste
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v, 'l=', l, 'u=', u, 'e=', e, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops' 
      
    
def workfactor_GBA_rep_tech_all_e_onelevel(n,K,k1,t,p,s):
    mincost=10000000000
    a=1
    M=floor((p^s)/2)
    for l in range(1,n-k1+1):
        print l
        for v in range(max(1, t- (n-k1-l)*M), min(t,(k1+l)*M)+1):
            sp= countLee(n,t,p^s)*countLee(k1+l,v,p^s)^(-1)*countLee(n-k1-l,t-v,p^s)^(-1)
            for u in UnorderedTuples(range(l), a-1).list():
                for b in range(0, 2*(k1+l)*M-v+1):
                    e=[b]
                    cs = rep_tech_copy(a,u,e,k1,l,v,p,s)
                    it=cs[0]+((n-k1-l)^2*(n+1)+ cs[1]*(n-k1)*min((n-k1-l), Leeaverage(p,s)^(-1)*(t-v+1)))*(log(p^s,2)+log(p^s,2)^2)
                    cost=N(log(it*sp,2))
                    if cost<mincost:
                        bestv=v
                        bestl=l
                        bestu=u
                        beste=e
                        mincost=copy(cost)
        print bestv,beste,mincost
    cost=copy(mincost)
    v=bestv
    l=bestl
    u=bestu
    e=beste
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v, 'l=', l, 'u=', u, 'e=', e, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'
    

def workfactor_GBA_rep_tech_all_e_twolevels(n,K,k1,t,p,s):
    mincost=10000000000
    a=2
    M=floor((p^s)/2)
    for l in range(1,n-k1+1):
        for v in range(max(1, t- (n-k1-l)*M), min(t,(k1+l)*M)+1):
            sp= countLee(n,t,p^s)*countLee(k1+l,v,p^s)^(-1)*countLee(n-k1-l,t-v,p^s)^(-1)
            for u in UnorderedTuples(range(l), a-1).list():
                for b in range(0, 2*(k1+l)*M-v+1):
                    for c in range(0,4*(k1+l)*M-v-2*b+1):
                        e=[b,c]
                        cs = rep_tech(a,u,e,k1,l,v,p,s)
                        it=cs[0]+((n-k1-l)^2*(n+1)+ cs[1]*(n-k1)*min((n-k1-l), Leeaverage(p,s)^(-1)*(t-v+1)))*(log(p^s,2)+log(p^s,2)^2)
                        cost=N(log(it*sp,2))
                        if cost<mincost:
                            bestv=v
                            bestl=l
                            bestu=u
                            beste=e
                            mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    u=bestu
    e=beste
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v, 'l=', l, 'u=', u, 'e=', e, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'       
    
    
def workfactor_GBA_rep_restricted_onelevel(n,K,k1,t,p,s):
    mincost=10000000000
    M=floor((p^s)/2)
    a=1
    for l in range(1,n-k1+1):
        print l
        for v in range(max(1, t- (n-k1-l)*M), min(t,(k1+l)*M)+1):
            if 2*countLee_copy(k1+l,v,p^s)/(p^(s*l))<2^40:
                sp= countLee_copy(n,t,p^s)*countLee_copy(k1+l,v,p^s)^(-1)*countLee_copy(n-k1-l,t-v,p^s)^(-1)
                for b in [0,2*(k1+l)*M-v]:
                    e=[b]
                    for u in UnorderedTuples(range(l), a-1).list():
                        if countLee_copy(k1+l,floor((v+b)/2),p^s)<2^40:
                            cs = rep_tech_copy(a,u,e,k1,l,v,p,s)
                            it=cs[0]+((n-k1-l)^2*(n+1)+ cs[1]*(n-k1)*min((n-k1-l), Leeaverage(p,s)^(-1)*(t-v+1)))*(log(p^s,2)+log(p^s,2)^2)
                            cost=N(log(it*sp,2))
                            if cost<mincost:
                                bestv=v
                                bestl=l
                                bestu=u
                                beste=e
                                mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    u=bestu
    e=beste
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v, 'l=', l, 'u=', u, 'e=', e, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'  
    
    
    
    
def workfactor_GBA_rep_restricted_twolevel(n,K,k1,t,p,s):
    mincost=10000000000
    M=floor((p^s)/2)
    a=2
    for l in range(1,n-k1+1):
        print l
        for v in range(max(1, t- (n-k1-l)*M), min(t,(k1+l)*M)+1):
            if countLee_copy(k1+l,v,p^s)/(p^(s*l))<2^40:
                sp= countLee_copy(n,t,p^s)*countLee_copy(k1+l,v,p^s)^(-1)*countLee_copy(n-k1-l,t-v,p^s)^(-1)
                for b in [0,2*(k1+l)*M-v]:
                    for c in [0,4*(k1+l)*M-v-2*b]:
                        e=[b,c]
                        for u in UnorderedTuples(range(l), a-1).list():
                            if max(2*countLee_copy(k1+l, floor((v+b)/2),p^s)/(p^(s*u[0])), 4*countLee_copy(k1+l, floor(v/4+ b/2+c/4),p^s))<2^40:
                                cs = rep_tech_copy(a,u,e,k1,l,v,p,s)
                                it=cs[0]+((n-k1-l)^2*(n+1)+ cs[1]*(n-k1)*min((n-k1-l), Leeaverage(p,s)^(-1)*(t-v+1)))*(log(p^s,2)+log(p^s,2)^2)
                                cost=N(log(it*sp,2))
                                if cost<mincost:
                                    bestv=v
                                    bestl=l
                                    bestu=u
                                    beste=e
                                    mincost=copy(cost)
    cost=copy(mincost)
    v=bestv
    l=bestl
    u=bestu
    e=beste
    KS=N(log(p^s,2)*K*(n-K)+log(p^(s-1),2)*K*(K-k1))
    print 'Given n= ', n, 'k1=', k1, 'K=', K,   't=',t, 'v= ',v, 'l=', l, 'u=', u, 'e=', e, 'KS=', KS
    print 'cost= 2^',cost, 'bit ops'                   
